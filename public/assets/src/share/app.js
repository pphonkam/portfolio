import './app.sass'

$(() => {
  const nav = $('#landing .navbar');
  const navLink = nav.find('.nav-link');
  const element = ['#landing', '#about','#portfolio','#contact'];
  let max = element.length;
  let fixed = false;

  for(var i=0;i<max;i++){
    element[i] = $(element[i]);
  }

  function scrollToID(id) {
    let pY = $(id).offset().top;
    $('html,body').animate({ scrollTop:pY },800);
  }

  navLink.click(function(e) {
    e.preventDefault();
    scrollToID($(this).attr('href'));
  });

  $('#view-my-works').click(function(e) {
    e.preventDefault();
    scrollToID('#about');
  });

  function viewPage(a) {
    let b = a;
    for (var i = max-1; i>=0; i--){
      if(element[i].offset().top<b)return i;
    }
    return -1;
  }

  function dragScroll() {
    let top = $(window).scrollTop();
    let checkPoint = $('#about').offset().top - nav.height();
    let index = viewPage(top + nav.height());
    const navIndex = (index==-1)?0:index;

    nav.find('.active').removeClass('active');
    $(navLink[navIndex]).addClass('active');

    if(top > checkPoint) {
      nav.addClass('fixed');
      fixed = true;
    } else if (top < checkPoint || top == 0) {
      nav.removeClass('fixed');
      fixed = false;
    }
  }

  dragScroll();
  $(window).scroll(dragScroll).resize(function(){
    dragScroll();
  });

  const $header = $('#header');
  const $hamburger = $header.find($('.hamburger'));

  $hamburger.click(function() {
    if($header.hasClass('is-active')) {
      $header.removeClass('is-active');
      $hamburger.removeClass('is-active');
    } else {
      $header.addClass('is-active');
      $hamburger.addClass('is-active');
    }
  });

  $header.find($('.nav-link')).click(function() {
    if($header.hasClass('is-active')) {
      $header.removeClass('is-active');
      $hamburger.removeClass('is-active');
    }
  });
});
