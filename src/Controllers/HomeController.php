<?php

namespace App\Controllers;

use App\Models\About;
use App\Models\Project;
use App\Models\Skill;

class HomeController extends BaseController
{
    public function index($request, $response) {

        $about = About::all();
        $projects = Project::where('status', '=', '1')->get();
        $skills = Skill::all();

        return $this->container->view->render($response, 'home/main.twig', [
            'about' => $about,
            'projects' => $projects,
            'skills' => $skills
        ]);

    }
}
