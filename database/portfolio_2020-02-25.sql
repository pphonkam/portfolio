# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.3.12-MariaDB)
# Database: portfolio
# Generation Time: 2020-02-25 06:37:44 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table about
# ------------------------------------------------------------

DROP TABLE IF EXISTS `about`;

CREATE TABLE `about` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `icon` char(100) DEFAULT NULL,
  `title` char(100) DEFAULT NULL,
  `detail` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `about` WRITE;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;

INSERT INTO `about` (`id`, `icon`, `title`, `detail`)
VALUES
	(1,'mobile','Responsive','Responsive to all devices, desktop, tablet and mobile.'),
	(2,'cubes','CSS Framework','Build by css frameworks bootstrap, bulma and another.'),
	(3,'css3','SASS/LESS','Using sass,scss,less for dynamic code to style sheet.'),
	(4,'coffee','JavaScript','javascript/Jquery to build interactive site.');

/*!40000 ALTER TABLE `about` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(50) DEFAULT NULL,
  `description` char(100) DEFAULT NULL,
  `thumbnail` char(50) DEFAULT NULL,
  `type` char(20) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `link` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;

INSERT INTO `projects` (`id`, `title`, `description`, `thumbnail`, `type`, `status`, `link`)
VALUES
	(1,'Property Perfect',NULL,'property-perfect','Front-end Developer',1,'https://www.pf.co.th/th/home'),
	(2,'Tesco Lotus App',NULL,'tescolotus-app','Front-end Developer',1,'https://www.tescolotus.com/app'),
	(3,'Tesco Lotus Horoscope',NULL,'tescolotus-horoscope','Front-end Developer',1,'https://www.tescolotus.com/horoscope'),
	(4,'Dumex e-learning',NULL,'dumex-e-learning','Front-end Developer',1,'http://www.dumexeln-learning.com/login'),
	(5,'Meiji Museum',NULL,'meiji-museum','Front-end Developer',1,'https://cpmeiji.com/milkmuseum/th'),
	(6,'Sansiri The Monument',NULL,'sansiri-the-monument','Front-end Developer',1,'https://www.sansiri.com/condominium/the-monument-thong-lo/th/'),
	(7,'Homeplace',NULL,'homeplace','Front-end Developer',1,'http://www.homeplace.co.th/th/'),
	(8,'เมืองไทย ประกันชีวิต',NULL,'muangthai','Front-end Developer',1,'https://www.muangthai.co.th/about');

/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table skills
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skills`;

CREATE TABLE `skills` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) DEFAULT NULL,
  `level` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;

INSERT INTO `skills` (`id`, `title`, `level`)
VALUES
	(1,'HTML',80),
	(2,'CSS',85),
	(3,'JavaScript',65),
	(4,'Photoshop',75),
	(5,'Illustrator',60),
	(6,'Mysql',30),
	(7,'PHP',30),
	(8,'Vue.js',40),
	(9,'Wordpress',60);

/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
